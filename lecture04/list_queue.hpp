#pragma once

template <typename T>
class list_queue {
    protected:
        struct Node {
            Node *next;
            Node *prev;
            T     data;
        };

        Node head;
        Node tail;

    public:
        list_queue() : head({&tail, &tail}), tail({&head, &head}) {}
        const T& front() { return head.next->data; };
        const T& back()  { return tail.prev->data; };

        void push(const T &data) {
            Node *node	    = new Node({&tail, tail.prev, data});
            tail.prev->next = node;
            tail.prev       = node;
	}
        void pop() {
            Node *old_front = head.next;
            Node *new_front = head.next->next;

            new_front->prev = &head;
            head.next       = new_front;

            delete old_front;
	}
};
