// duplicates_set.cpp

#include <algorithm>
#include <climits>
#include <iostream>
#include <random>
#include <set>

using namespace std;

const int N = 1<<15;

int main(int argc, char *argv[]) {
    random_device		rd;
    default_random_engine	g(rd());
    uniform_int_distribution<>	d(1, INT_MAX);
    set<int>			s;

    for (int i = 0; i < N; i++) {
    	int n = d(g);
    	if (s.find(n) != s.end()) {
    	    cout << n << " is duplicated" << endl;
    	    break;
    	}
    	s.insert(n);
    }

    return 0;
}
